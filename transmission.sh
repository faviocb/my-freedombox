# Plinth -> Apps => install Transmission
# Plinth -> Apps-> Transmission 
# default "Download directory" path is /var/lib/transmission-daemon/downloads

#--------------------------------------
# SSH and create sub folder "Curated"
#-------------------------------------
# ssh ...
cd /var/lib/transmission-daemon/downloads
mkdir curated
sudo chown debian-transmission curated

# Plinth -> Apps-> Transmission => set Subdirectory to "curated". Click Update setup.


#-------------------------
# Sharing curated folder
#-------------------------

# Plinth -> Apps-> Sharing => Add share:
#  Name of share: torrent
#  Path to share: /var/lib/transmission-daemon/downloads/curated
#  User groups that can read the files in the share:
#    -  Access to the private shares (freedombox-share)


